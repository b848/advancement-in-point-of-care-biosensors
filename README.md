**Advancement in Point-of-care biosensors**

|Discipline|Biology|
| :- | :- |
|Theme|Bio Material|
|Module|Advancement in Point-of-care biosensors|


**About the module:**

Advancement in Point-of-care biosensors 

**Target Audience:**

Class 8 to 12

**Course alignment:**

Physics, Biology,Chemistry, Material Science 

**Boards mapped:**

CBSE, ISCE, State Boards

**Language:**

English


|Name of Developer/ principal investigator|Dr. Satanand Mishra|
| :- | :- |
|Institute|CSIR-AMPRI|
|Email. ID|snmishra07@gmail.com|
|Department|CARS & GM|

Contributor List

|Sr. No.|Name |Department|Institute|Email id|
| :- | :- | :- | :- | :- |
|1|Dr. Chetna Dhand|<p>ALLOYS, COMPOSITES AND CELLULAR MATERIALS</p><p></p>|CSIR-AMPRI, Bhopal|chetna.dhand@ampri.res.in|


